from . import db, login_manager
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin


class search_hist(db.Model):
	__tablename__ = 'serch_hist'
	id = db.Column(db.Integer, primary_key = True)
	user_name = db.Column(db.String(99))
	query_text = db.Column(db.Text)
	query_date = db.Column(db.Date)
	query_time = db.Column(db.Time)

	def __init__(self, user_name, query_text, query_date, query_time):
		self.user_name = user_name
		self.query_text = query_text
		self.query_date = query_date
		self.query_time = query_time


class Permission:
    FOLLOW = 1
    COMMENT = 2
    WRITE = 4
    MODERATE = 8
    ADMIN = 16

class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='dynamic')

    def __init__(self, **kwargs):
        super(Role, self).__init__(**kwargs)
        if self.permissions is None:
            self.permissions = 0

    @staticmethod
    def insert_roles():
        roles = {
            'User': [Permission.FOLLOW, Permission.COMMENT, Permission.WRITE],
            'Moderator': [Permission.FOLLOW, Permission.COMMENT,
                          Permission.WRITE, Permission.MODERATE],
            'Administrator': [Permission.FOLLOW, Permission.COMMENT,
                              Permission.WRITE, Permission.MODERATE,
                              Permission.ADMIN],
        }
        default_role = 'User'
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.reset_permissions()
            for perm in roles[r]:
                role.add_permission(perm)
            role.default = (role.name == default_role)
            db.session.add(role)
        db.session.commit()

    def add_permission(self, perm):
        if not self.has_permission(perm):
            self.permissions += perm

    def remove_permission(self, perm):
        if self.has_permission(perm):
            self.permissions -= perm

    def reset_permissions(self):
        self.permissions = 0

    def has_permission(self, perm):
        return self.permissions & perm == perm

    def __repr__(self):
        return '<Role %r>' % self.name

class User(db.Model, UserMixin):
	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key = True)
	user_name = db.Column(db.String(99), unique = True, index = True)
	password_hash = db.Column(db.String(99))
	role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))

	def __repr__(self):
		return '%r' % self.user_name

	@property
	def password(self):
		raise AttributeError('password is not readeble attribute')

	@password.setter
	def password(self, password):
		self.password_hash = generate_password_hash(password)

	def verify_password(self, password):
		return check_password_hash(self.password_hash, password)

	def can(self, perm):
		return self.role is not None and self.role.has_permission(perm)

	def is_administrator(self):
		return self.can(Permission.ADMIN)

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class sp_per(db.Model):
	__tablename__ = 'sp_per'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.Text)
	inf = db.Column(db.Text)
	dop_inf = db.Column(db.Text)
	main = db.Column(db.Text)
	born_in = db.Column(db.Text)
	birthday = db.Column(db.Text)
	passport = db.Column(db.Text)
	place = db.Column(db.Text)
	object_type = db.Column(db.Text)

	def __init__(self, name, inf, dop_inf, main, born_in, birthday, passport, place, object_type):
		self.name = name
		self.inf = inf
		self.dop_inf = dop_inf
		self.main = main
		self.born_in = born_in
		self.birthday = birthday
		self.passport = passport
		self.place = place
		self.object_type = object_type

	def __repr__(self):
		return '%r_*_*_%r_*_*_%r_*_*_%r_*_*_%r_*_*_%r_*_*_%r_*_*_%r_*_*_%r' % (self.name, self.inf, self.dop_inf, self.main, self.born_in, self.birthday, self.passport, self.place, self.object_type)


class mir_sudy(db.Model):
	__tablename__ = 'mir_sudy'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	name = db.Column(db.Text)
	uchastok = db.Column(db.Text)
	date = db.Column(db.Text)
	note = db.Column(db.Text)

	def __init__(self, number, name, uchastok, date, note):
		self.number = number
		self.name = name
		self.uchastok = uchastok
		self.date = date
		self.note = note

	def __repr__(self):
		return '%r_*_*_%r_*_*_%r_*_*_%r_*_*_%r' % (self.number, self.name, self.uchastok, self.date, self.note)

class ug_Sovet(db.Model):
	__tablename__ = 'ug_Sovet'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	name = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, name, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.name = name
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.place = more_inf

class ug_Promysh(db.Model):
	__tablename__ = 'ug_Promysh'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	name = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, name, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.name = name
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class ug_Prigorod(db.Model):
	__tablename__ = 'ug_Prigorod'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	name = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, name, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.name = name
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class ug_Prvoberezh(db.Model):
	__tablename__ = 'ug_Prvoberezh'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	name = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, name, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.name = name
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class ug_Mozdok(db.Model):
	__tablename__ = 'ug_Mozdok'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	name = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, name, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.name = name
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class ug_Lenin(db.Model):
	__tablename__ = 'ug_Lenin'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	name = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, name, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.name = name
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class ug_Kirov(db.Model):
	__tablename__ = 'ug_Kirov'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	name = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, name, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.name = name
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class ug_Iraf(db.Model):
	__tablename__ = 'ug_Iraf'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	name = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, name, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.name = name
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class ug_Digor(db.Model):
	__tablename__ = 'ug_Digor'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	name = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, name, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.name = name
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class ug_Ardon(db.Model):
	__tablename__ = 'ug_Ardon'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	name = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, name, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.name = name
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class ug_Alagir(db.Model):
	__tablename__ = 'ug_Alagir'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	name = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, name, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.name = name
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class sud_Al(db.Model):
	__tablename__ = 'sud_Al'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	category = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, category, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.category = category
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class sud_Ard(db.Model):
	__tablename__ = 'sud_Ard'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	category = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, category, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.category = category
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class sud_Verh_i_Vl_Gar(db.Model):
	__tablename__ = 'sud_Verh_i_Vl_Gar'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	category = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, category, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.category = category
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class sud_Dig(db.Model):
	__tablename__ = 'sud_Dig'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	category = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, category, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.category = category
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class sud_Iraf(db.Model):
	__tablename__ = 'sud_Iraf'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	category = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, category, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.category = category
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class sud_Kir(db.Model):
	__tablename__ = 'sud_Kir'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	category = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, category, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.category = category
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class sud_Len(db.Model):
	__tablename__ = 'sud_Len'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	category = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, category, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.category = category
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class sud_Mozd(db.Model):
	__tablename__ = 'sud_Mozd'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	category = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, category, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.category = category
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class sud_Prav(db.Model):
	__tablename__ = 'sud_Prav'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	category = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, category, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.category = category
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class sud_Prig(db.Model):
	__tablename__ = 'sud_Prig'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	category = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, category, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.category = category
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class sud_Prom(db.Model):
	__tablename__ = 'sud_Prom'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	category = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, category, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.category = category
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class sud_Sov(db.Model):
	__tablename__ = 'sud_Sov'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	category = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, category, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.category = category
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class sud_pr_iski_Sov(db.Model):
	__tablename__ = 'sud_pr_iski_Sov'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	date_01 = db.Column(db.Text)
	category = db.Column(db.Text)
	some_inf = db.Column(db.Text)
	date_02 = db.Column(db.Text)
	comment = db.Column(db.Text)
	date_03 = db.Column(db.Text)
	more_inf = db.Column(db.Text)

	def __init__(self, number, date_01, category, some_inf, date_02, comment, date_03, more_inf):
		self.number = number
		self.date_01 = date_01
		self.category = category
		self.some_inf = some_inf
		self.date_02 = date_02
		self.comment = comment
		self.date_03 = date_03
		self.more_inf = more_inf

class fio_noname(db.Model):
	__tablename__ = 'fio_noname'
	id = db.Column(db.Integer, primary_key=True)
	otvetchik = db.Column(db.Text)
	istec = db.Column(db.Text)
	category = db.Column(db.Text)
	date = db.Column(db.Text)

	def __init__(self, otvetchik, istec, category, date):
		self.otvetchik = otvetchik
		self.istec = istec
		self.category = category
		self.date = date

class fio_ugolovniki(db.Model):
	__tablename__ = 'fio_ugolovniki'
	id = db.Column(db.Integer, primary_key=True)
	name01 = db.Column(db.Text)
	name02 = db.Column(db.Text)
	name03 = db.Column(db.Text)
	date = db.Column(db.Text)

	def __init__(self, name01, name02, name03, date):
		self.name01 = name01
		self.name02 = name02
		self.name03 = name03
		self.date = date

class fio_Renesans(db.Model):
	__tablename__ = 'fio_Renesans'
	id = db.Column(db.Integer, primary_key=True)
	name01 = db.Column(db.Text)
	name02 = db.Column(db.Text)
	name03 = db.Column(db.Text)
	number = db.Column(db.Text)
	summ = db.Column(db.Text)
	date = db.Column(db.Text)
	col_g = db.Column(db.Text)
	col_h = db.Column(db.Text)
	col_i = db.Column(db.Text)

	def __init__(self, name01, name02, name03, number, summ, 
						date, col_g, col_h, col_i):
		self.name01 = name01
		self.name02 = name02
		self.name03 = name03
		self.number = number
		self.summ = summ
		self.date = date
		self.col_g = col_g
		self.col_h = col_h
		self.col_i = col_i

class fio_Alfa(db.Model):
	__tablename__ = 'fio_Alfa'
	id = db.Column(db.Integer, primary_key=True)
	fio = db.Column(db.Text)
	prezhnee_fio = db.Column(db.Text)
	summa_zayavki = db.Column(db.Text)
	dni_prosrochki = db.Column(db.Text)
	summa_osn_dolga = db.Column(db.Text)
	summa_prosroch_dolga = db.Column(db.Text)
	data_postupl_zayavki = db.Column(db.Text)

	def __init__(self, fio, prezhnee_fio, summa_zayavki, dni_prosrochki, summa_osn_dolga, summa_prosroch_dolga, 
																		data_postupl_zayavki):
		self.fio = fio
		self.prezhnee_fio = prezhnee_fio
		self.summa_zayavki = summa_zayavki
		self.dni_prosrochki = dni_prosrochki
		self.summa_osn_dolga = summa_osn_dolga
		self.summa_prosroch_dolga = summa_prosroch_dolga
		self.data_postupl_zayavki = data_postupl_zayavki

class fio_BRR(db.Model):
	__tablename__ = 'fio_BRR'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.Text)
	summ = db.Column(db.Text)

	def __init__(self, name, summ):
		self.name = name
		self.summ = summ

class fio_rozysk_avto(db.Model):
	__tablename__ = 'fio_rozysk_avto'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	name = db.Column(db.Text)
	pistav01 = db.Column(db.Text)
	UFSSP_OSP = db.Column(db.Text)
	data_zaveden_dela = db.Column(db.Text)
	nomer_dela = db.Column(db.Text)
	pristav02 = db.Column(db.Text)
	data_zapret_snyat_s_ucheta = db.Column(db.Text)
	marka_model = db.Column(db.Text)
	gos_nomer = db.Column(db.Text)
	VIN = db.Column(db.Text)
	nomer_kuzova = db.Column(db.Text)
	cvet = db.Column(db.Text)
	podrazdel_gibdd = db.Column(db.Text)
	rekvisity = db.Column(db.Text)

	def __init__(self, number, name, pistav01, UFSSP_OSP, data_zaveden_dela, nomer_dela, pristav02, 
					data_zapret_snyat_s_ucheta, 
					marka_model, gos_nomer, VIN, nomer_kuzova, cvet, podrazdel_gibdd, rekvisity):
		self.number = number
		self.name = name
		self.pistav01 = pistav01
		self.UFSSP_OSP = UFSSP_OSP
		self.data_zaveden_dela = data_zaveden_dela
		self.nomer_dela = nomer_dela
		self.pristav02 = pristav02
		self.data_zapret_snyat_s_ucheta = data_zapret_snyat_s_ucheta
		self.marka_model = marka_model
		self.gos_nomer = gos_nomer
		self.VIN = VIN
		self.nomer_kuzova = nomer_kuzova
		self.cvet = cvet
		self.podrazdel_gibdd = podrazdel_gibdd
		self.rekvisity = rekvisity

class fio_rozysk(db.Model):
	__tablename__ = 'fio_rozysk'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	name = db.Column(db.Text)
	birthday = db.Column(db.Text)
	mesto_rozhd = db.Column(db.Text)
	cont_tel = db.Column(db.Text)
	otdel_pristavov = db.Column(db.Text)

	def __init__(self, number, name, birthday, mesto_rozhd, cont_tel, otdel_pristavov):
		self.number = number
		self.name = name
		self.birthday = birthday
		self.mesto_rozhd = mesto_rozhd
		self.cont_tel = cont_tel
		self.otdel_pristavov = otdel_pristavov

class fio_grant01(db.Model):
	__tablename__ = 'fio_grant01'
	id = db.Column(db.Integer, primary_key=True)
	company01 = db.Column(db.Text)
	deyatelnost = db.Column(db.Text)
	company02 = db.Column(db.Text)

	def __init__(self, company01, deyatelnost, company02):
		self.company01 = company01
		self.deyatelnost = deyatelnost
		self.company02 = company02

class fio_grant02(db.Model):
	__tablename__ = 'fio_grant02'
	id = db.Column(db.Integer, primary_key=True)
	num01 = db.Column(db.Text)
	num02 = db.Column(db.Text)
	name = db.Column(db.Text)
	company = db.Column(db.Text)
	goal = db.Column(db.Text)
	contacts = db.Column(db.Text)

	def __init__(self, num01, num02, name, company, goal, contacts):
		self.num01 = num01
		self.num02 = num02
		self.name = name
		self.company = company
		self.goal = goal
		self.contacts = contacts

class fio_grant03(db.Model):
	__tablename__ = 'fio_grant03'
	id = db.Column(db.Integer, primary_key=True)
	number = db.Column(db.Text)
	col01 = db.Column(db.Text)
	col02 = db.Column(db.Text)
	col03 = db.Column(db.Text)
	col04 = db.Column(db.Text)

	def __init__(self, number, col01, col02, col03, col04):
		self.number = number
		self.col01 = col01
		self.col02 = col02
		self.col03 = col03
		self.col04 = col04

class fio_otkaz01(db.Model):
	__tablename__ = 'fio_otkaz01'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.Text)
	col01 = db.Column(db.Text)
	col02 = db.Column(db.Text)
	col03 = db.Column(db.Text)

	def __init__(self, name, col01, col02, col03):
		self.name = name
		self.col01 = col01
		self.col02 = col02
		self.col03 = col03

class fio_otkaz02(db.Model):
	__tablename__ = 'fio_otkaz02'
	id = db.Column(db.Integer, primary_key=True)
	date = db.Column(db.Text)
	name = db.Column(db.Text)
	col01 = db.Column(db.Text)
	col02 = db.Column(db.Text)
	col03 = db.Column(db.Text)
	col04 = db.Column(db.Text)
	col05 = db.Column(db.Text)

	def __init__(self, date, name, col01, col02, col03, col04, col05):
		self.date = date
		self.name = name
		self.col01 = col01
		self.col02 = col02
		self.col03 = col03
		self.col04 = col04
		self.col05 = col05

class fio_digbank(db.Model):
	__tablename__ = 'fio_digbank'
	id = db.Column(db.Integer, primary_key=True)
	naimen_imushestva = db.Column(db.Text)
	buh_otchet = db.Column(db.Text)
	faktich_nalich = db.Column(db.Text)

	def __init__(self, naimen_imushestva, buh_otchet, faktich_nalich):
		self.naimen_imushestva = naimen_imushestva
		self.buh_otchet = buh_otchet
		self.faktich_nalich = faktich_nalich

class fio_rosfin(db.Model):
	__tablename__ = 'fio_rosfin'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.Text)
	number = db.Column(db.Text)
	place = db.Column(db.Text)

	def __init__(self, name, number, place):
		self.name = name
		self.number = number
		self.place = place