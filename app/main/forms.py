from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

class QueryForm(FlaskForm):
	surname = StringField('', validators = [DataRequired()])
	submit = SubmitField('Submit')

class IdentQueryForm(FlaskForm):
	identifier = StringField('', validators = [DataRequired()])
	submit = SubmitField('Submit')