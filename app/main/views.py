from datetime import datetime
from flask import render_template, session, redirect, url_for, current_app, jsonify
from . import main
from .forms import QueryForm, IdentQueryForm
from .. import db
from ..email import send_email
from ..models import search_hist, User, Role, sp_per, mir_sudy
from ..table import TestTable, Logs_Table, Sp_per_Table, Mir_sudy_Table
from ..models import ug_Sovet, ug_Promysh, ug_Prigorod, ug_Prvoberezh, ug_Mozdok, ug_Lenin, ug_Kirov, ug_Iraf, ug_Digor, ug_Ardon, ug_Alagir
from ..table import Ug_Sovet_Table, Ug_Promysh_Table, Ug_Prigorod_Table, Ug_Prvoberezh_Table, Ug_Mozdok_Table, Ug_Lenin_Table
from ..table import Ug_Kirov_Table, Ug_Iraf_Table, Ug_Digor_Table, Ug_Ardon_Table, Ug_Alagir_Table
from ..models import sud_Al, sud_Ard, sud_Verh_i_Vl_Gar, sud_Dig, sud_Iraf, sud_Kir, sud_Len, sud_Mozd, sud_Prav, sud_Prig, sud_Prom
from ..models import sud_Sov, sud_pr_iski_Sov
from ..table import Sud_Al_Table, Sud_Ard_Table, Sud_Verh_i_Vl_Gar_Table, Sud_Dig_Table, Sud_Iraf_Table, Sud_Kir_Table, Sud_Len_Table
from ..table import Sud_Mozd_Table, Sud_Prav_Table, Sud_Prig_Table, Sud_Prom_Table, Sud_Sov_Table, Sud_pr_iski_Sov_Table
from ..models import fio_noname, fio_ugolovniki, fio_Renesans, fio_Alfa, fio_BRR, fio_rozysk_avto, fio_rozysk, fio_grant01, fio_grant02
from ..models import fio_grant03, fio_otkaz01, fio_otkaz02, fio_digbank, fio_rosfin
from ..table import Fio_noname_Table, Fio_ugolovniki_Table, Fio_Renesans_Table, Fio_Alfa_Table, Fio_BRR_Table, Fio_rozysk_avto_Table
from ..table import Fio_rozysk_Table, Fio_grant01_Table, Fio_grant02_Table, Fio_grant03_Table, Fio_otkaz01_Table, Fio_otkaz02_Table
from ..table import Fio_digbank_Table, Fio_rosfin_Table
import flask_wtf
from flask import request
import pandas as pd
from flask_login import current_user, login_required
from ..decorators import admin_required
import time
from . import sv
import os

@main.route('/')
def index():
	return render_template('index.html')


@main.route('/search', methods = ['GET','POST'])
@login_required
def search():
	form = QueryForm()
	if form.validate_on_submit():
		fio = form.surname.data
		fio = fio.lower()
		fio_list = fio.split()
		if len(fio_list) < 3:
			return render_template('search.html', form = form)
		#################################
		user_id = current_user.get_id()
		user_name = db.session.query(User.user_name, User.password_hash).filter_by(id = user_id).first()[0]
		now_unix = time.time()
		query_date = time.strftime("%Y-%m-%d",time.gmtime(now_unix+60*60*3))
		query_time = time.strftime("%H:%M:%S",time.gmtime(now_unix+60*60*3))
		sh_row = search_hist(user_name = user_name, query_text = fio, query_date = query_date, query_time = query_time)
		db.session.add(sh_row)
		db.session.commit()
		time.sleep(1.1)
		user_queries = db.session.query(search_hist.user_name).filter_by(user_name = user_name, query_date = query_date).all()
		q_num = len(user_queries)
		if q_num == 6:
			msg_body = 'Более 5 запросов от пользователя: ' + user_name
			send_email('Khaev@kpk-sks.ru', 'Много_запросов_к_базе!!!', msg_body)
		###############################
		fio_vars = [fio_list[0]+' '+fio_list[1][0]]
		###############################
		show_tables = []
		session = db.session()
		###############################
		for tabcol in [
		[sp_per, Sp_per_Table, sp_per.name], [mir_sudy, Mir_sudy_Table, mir_sudy.name],
		[ug_Sovet, Ug_Sovet_Table, ug_Sovet.name], [ug_Promysh, Ug_Promysh_Table, ug_Promysh.name],
		[ug_Prigorod, Ug_Prigorod_Table, ug_Prigorod.name], [ug_Prvoberezh, Ug_Prvoberezh_Table, ug_Prvoberezh.name],
		[ug_Mozdok, Ug_Mozdok_Table, ug_Mozdok.name], [ug_Lenin, Ug_Lenin_Table, ug_Lenin.name],
		[ug_Kirov, Ug_Kirov_Table, ug_Kirov.name], [ug_Iraf, Ug_Iraf_Table, ug_Iraf.name],
		[ug_Digor, Ug_Digor_Table, ug_Digor.name], [ug_Ardon, Ug_Ardon_Table, ug_Ardon.name],
		[ug_Alagir, Ug_Alagir_Table, ug_Alagir.name], [sud_Al, Sud_Al_Table, sud_Al.category],
		[sud_Ard, Sud_Ard_Table, sud_Ard.category], [sud_Verh_i_Vl_Gar, Sud_Verh_i_Vl_Gar_Table, sud_Verh_i_Vl_Gar.category],
		[sud_Dig, Sud_Dig_Table, sud_Dig.category], [sud_Iraf, Sud_Iraf_Table, sud_Iraf.category],
		[sud_Kir, Sud_Kir_Table, sud_Kir.category], [sud_Len, Sud_Len_Table, sud_Len.category],
		[sud_Mozd, Sud_Mozd_Table, sud_Mozd.category], [sud_Prav, Sud_Prav_Table, sud_Prav.category],
		[sud_Prig, Sud_Prig_Table, sud_Prig.category], [sud_Prom, Sud_Prom_Table, sud_Prom.category],
		[sud_Sov, Sud_Sov_Table, sud_Sov.category], [sud_pr_iski_Sov, Sud_pr_iski_Sov_Table, sud_pr_iski_Sov.category],
		[fio_noname, Fio_noname_Table, fio_noname.otvetchik], [fio_noname, Fio_noname_Table, fio_noname.istec], 
		[fio_ugolovniki, Fio_ugolovniki_Table, fio_ugolovniki.name01],
		[fio_Alfa, Fio_Alfa_Table, fio_Alfa.fio], [fio_Alfa, Fio_Alfa_Table, fio_Alfa.prezhnee_fio],
		[fio_BRR, Fio_BRR_Table, fio_BRR.name], [fio_rozysk_avto, Fio_rozysk_avto_Table, fio_rozysk_avto.name],
		[fio_rozysk, Fio_rozysk_Table, fio_rozysk.name], [fio_grant01, Fio_grant01_Table, fio_grant01.company01],
		[fio_grant01, Fio_grant01_Table, fio_grant01.company02], [fio_grant02, Fio_grant02_Table, fio_grant02.name],
		[fio_grant02, Fio_grant02_Table, fio_grant02.company], [fio_grant03, Fio_grant03_Table, fio_grant03.col01],
		[fio_grant03, Fio_grant03_Table, fio_grant03.col02], [fio_grant03, Fio_grant03_Table, fio_grant03.col03],
		[fio_grant03, Fio_grant03_Table, fio_grant03.col04], [fio_otkaz01, Fio_otkaz01_Table, fio_otkaz01.name],
		[fio_otkaz02, Fio_otkaz02_Table, fio_otkaz02.name], [fio_digbank, Fio_digbank_Table, fio_digbank.naimen_imushestva],
		[fio_rosfin, Fio_rosfin_Table, fio_rosfin.name]
		]:
			for name in fio_vars:
				x = session.query(tabcol[0]).filter(tabcol[2].contains(name))
				if not list(x):
					continue
				show_tables.append(tabcol[1](x))
		for tabcol in [[fio_Renesans, Fio_Renesans_Table, fio_Renesans.name01]]:
			for name in [fio_list[0]]:
				x = session.query(tabcol[0]).filter(tabcol[2].contains(name))
				if not list(x):
					continue
				show_tables.append(tabcol[1](x))
		if not show_tables:
			show_tables = ['По данным инициалам нет данных']
		return render_template('test_table.html', show_tables = show_tables)
	return render_template('search.html', form = form)


@main.route('/logs/<user_name>/<query_date>')
@login_required
@admin_required
def show_logs(user_name, query_date):
	user_queries = db.session.query(search_hist).filter_by(user_name = user_name, query_date = query_date).all()
	show_tables = [Logs_Table(user_queries)]
	return render_template('test_table.html', show_tables = show_tables)





######## Список перечня ##############################

@main.route('/import/sp_per/<query_type>', methods = ['GET','POST'])
@login_required
@admin_required
def import_sp_per(query_type):
	form = IdentQueryForm()
	if form.validate_on_submit():
		spreadsheet_id = form.identifier.data
		upd_frequency = int(os.environ.get('UPD_FREQUENCY'))
		tables = [sp_per]
		req_sheets = ['sp_per']
		req_columns = [
		['name', 'inf', 'dop_inf', 'main', 'born_in', 'birthday',
		'passport','place','object_type']
		]
		service = sv.google_sheet_api_init()
		if not sv.check_file(service, spreadsheet_id, req_sheets, req_columns):
			return redirect(url_for('.import_sp_per', query_type = 'wrong_file'))
		query_type = 'data_loaded'
		tabs_data = []
		for i in range(len(req_sheets)):
			col_number = len(req_columns[i])
			range_name = 'A1:' + sv.get_excel_col_by_index(col_number)
			gs_rows = sv.get_google_sheet_rows(service, spreadsheet_id, req_sheets[i], range_name)
			db.session.query(tables[i]).delete()
			if len(gs_rows) == 1:
				tabs_data.append([req_sheets[i], 0])
				continue
			gs_rows = gs_rows[1:]
			try:
				sv.convert_rows_to_mysql(db, tables[i], gs_rows, col_number, upd_frequency)
			except:
				return redirect(url_for('.import_sp_per', query_type = 'wrong_file'))
			tabs_data.append([req_sheets[i], db.session.query(tables[i]).count()])
		tabs_number = len(tabs_data)
		return render_template('import_google_sheets.html', dataset_name = 'Список перечня', query_type = query_type,
										tabs_number = tabs_number, tabs_data = tabs_data)
	return render_template('import_google_sheets.html', form = form, dataset_name = 'Список перечня',
										query_type = query_type, tabs_number = 0, tabs_data = [])

################################################





######## Мировые суды ##############################

@main.route('/import/mir_sudy/<query_type>', methods = ['GET','POST'])
@login_required
@admin_required
def import_mir_sudy(query_type):
	form = IdentQueryForm()
	if form.validate_on_submit():
		spreadsheet_id = form.identifier.data
		upd_frequency = int(os.environ.get('UPD_FREQUENCY'))
		tables = [mir_sudy]
		req_sheets = ['ugolovniki']
		req_columns = [
		['number', 'name', 'uchastok', 'date', 'note']
		]
		service = sv.google_sheet_api_init()
		if not sv.check_file(service, spreadsheet_id, req_sheets, req_columns):
			return redirect(url_for('.import_mir_sudy', query_type = 'wrong_file'))
		query_type = 'data_loaded'
		tabs_data = []
		for i in range(len(req_sheets)):
			col_number = len(req_columns[i])
			range_name = 'A1:' + sv.get_excel_col_by_index(col_number)
			gs_rows = sv.get_google_sheet_rows(service, spreadsheet_id, req_sheets[i], range_name)
			db.session.query(tables[i]).delete()
			if len(gs_rows) == 1:
				tabs_data.append([req_sheets[i], 0])
				continue
			gs_rows = gs_rows[1:]
			try:
				sv.convert_rows_to_mysql(db, tables[i], gs_rows, col_number, upd_frequency)
			except:
				return redirect(url_for('.import_mir_sudy', query_type = 'wrong_file'))
			tabs_data.append([req_sheets[i], db.session.query(tables[i]).count()])
		tabs_number = len(tabs_data)
		return render_template('import_google_sheets.html', dataset_name = 'Мировые суды', query_type = query_type,
										tabs_number = tabs_number, tabs_data = tabs_data)
	return render_template('import_google_sheets.html', form = form, dataset_name = 'Мировые суды',
										query_type = query_type, tabs_number = 0, tabs_data = [])


################################################





######## Уголовники ##############################

@main.route('/import/ugolovniki/<query_type>', methods = ['GET','POST'])
@login_required
@admin_required
def import_ugolovniki(query_type):
	form = IdentQueryForm()
	if form.validate_on_submit():
		spreadsheet_id = form.identifier.data
		upd_frequency = int(os.environ.get('UPD_FREQUENCY'))
		tables = [ug_Sovet, ug_Promysh, ug_Prigorod, ug_Prvoberezh, ug_Mozdok, 
						ug_Lenin, ug_Kirov, ug_Iraf, ug_Digor, ug_Ardon, ug_Alagir]
		req_sheets = ['ug_Sovet', 'ug_Promysh', 'ug_Prigorod', 'ug_Prvoberezh', 'ug_Mozdok', 
						'ug_Lenin', 'ug_Kirov', 'ug_Iraf', 'ug_Digor', 'ug_Ardon', 'ug_Alagir']
		req_columns = [
		#ug_Sovet
		['number', 'date_01', 'name', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#ug_Promysh
		['number', 'date_01', 'name', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#ug_Prigorod
		['number', 'date_01', 'name', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#ug_Prvoberezh
		['number', 'date_01', 'name', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#ug_Mozdok
		['number', 'date_01', 'name', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#ug_Lenin
		['number', 'date_01', 'name', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#ug_Kirov
		['number', 'date_01', 'name', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#ug_Iraf
		['number', 'date_01', 'name', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#ug_Digor
		['number', 'date_01', 'name', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#ug_Ardon
		['number', 'date_01', 'name', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#ug_Alagir
		['number', 'date_01', 'name', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf']
		]
		service = sv.google_sheet_api_init()
		if not sv.check_file(service, spreadsheet_id, req_sheets, req_columns):
			return redirect(url_for('.import_ugolovniki', query_type = 'wrong_file'))
		query_type = 'data_loaded'
		tabs_data = []
		for i in range(len(req_sheets)):
			col_number = len(req_columns[i])
			range_name = 'A1:' + sv.get_excel_col_by_index(col_number)
			gs_rows = sv.get_google_sheet_rows(service, spreadsheet_id, req_sheets[i], range_name)
			db.session.query(tables[i]).delete()
			if len(gs_rows) == 1:
				tabs_data.append([req_sheets[i], 0])
				continue
			gs_rows = gs_rows[1:]
			try:
				sv.convert_rows_to_mysql(db, tables[i], gs_rows, col_number, upd_frequency)
			except:
				return redirect(url_for('.import_ugolovniki', query_type = 'wrong_file'))
			tabs_data.append([req_sheets[i], db.session.query(tables[i]).count()])
		tabs_number = len(tabs_data)
		return render_template('import_google_sheets.html', dataset_name = 'Уголовники', query_type = query_type,
										tabs_number = tabs_number, tabs_data = tabs_data)
	return render_template('import_google_sheets.html', form = form, dataset_name = 'Уголовники',
										query_type = query_type, tabs_number = 0, tabs_data = [])

################################################





######## Суды ##############################

@main.route('/import/sudy/<query_type>', methods = ['GET','POST'])
@login_required
@admin_required
def import_sudy(query_type):
	form = IdentQueryForm()
	if form.validate_on_submit():
		spreadsheet_id = form.identifier.data
		upd_frequency = int(os.environ.get('UPD_FREQUENCY'))
		tables = [sud_Al, sud_Ard, sud_Verh_i_Vl_Gar, sud_Dig, sud_Iraf, sud_Kir, sud_Len, sud_Mozd, 
						sud_Prav, sud_Prig, sud_Prom, sud_Sov, sud_pr_iski_Sov]
		req_sheets = ['sud_Al', 'sud_Ard', 'sud_Verh_i_Vl_Gar', 'sud_Dig', 'sud_Iraf', 'sud_Kir', 'sud_Len', 'sud_Mozd', 
						'sud_Prav', 'sud_Prig', 'sud_Prom', 'sud_Sov', 'sud_pr_iski_Sov']
		req_columns = [
		#sud_Al
		['number', 'date_01', 'category', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#sud_Ard
		['number', 'date_01', 'category', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#sud_Verh_i_Vl_Gar
		['number', 'date_01', 'category', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#sud_Dig
		['number', 'date_01', 'category', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#sud_Iraf
		['number', 'date_01', 'category', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#sud_Kir
		['number', 'date_01', 'category', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#sud_Len
		['number', 'date_01', 'category', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#sud_Mozd
		['number', 'date_01', 'category', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#sud_Prav
		['number', 'date_01', 'category', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#sud_Prig
		['number', 'date_01', 'category', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#sud_Prom
		['number', 'date_01', 'category', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#sud_Sov
		['number', 'date_01', 'category', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf'],
		#sud_pr_iski_Sov
		['number', 'date_01', 'category', 'some_inf', 'date_02', 'comment', 'date_03', 'more_inf']
		]
		service = sv.google_sheet_api_init()
		if not sv.check_file(service, spreadsheet_id, req_sheets, req_columns):
			return redirect(url_for('.import_sudy', query_type = 'wrong_file'))
		query_type = 'data_loaded'
		tabs_data = []
		for i in range(len(req_sheets)):
			col_number = len(req_columns[i])
			range_name = 'A1:' + sv.get_excel_col_by_index(col_number)
			gs_rows = sv.get_google_sheet_rows(service, spreadsheet_id, req_sheets[i], range_name)
			db.session.query(tables[i]).delete()
			if len(gs_rows) == 1:
				tabs_data.append([req_sheets[i], 0])
				continue
			gs_rows = gs_rows[1:]
			try:
				sv.convert_rows_to_mysql(db, tables[i], gs_rows, col_number, upd_frequency)
			except:
				return redirect(url_for('.import_sudy', query_type = 'wrong_file'))
			tabs_data.append([req_sheets[i], db.session.query(tables[i]).count()])
		tabs_number = len(tabs_data)
		return render_template('import_google_sheets.html', dataset_name = 'Суды', query_type = query_type,
										tabs_number = tabs_number, tabs_data = tabs_data)
	return render_template('import_google_sheets.html', form = form, dataset_name = 'Суды',
										query_type = query_type, tabs_number = 0, tabs_data = [])

################################################





######## По ФИО ##############################

@main.route('/import/fio/<query_type>', methods = ['GET','POST'])
@login_required
@admin_required
def import_fio(query_type):
	form = IdentQueryForm()
	if form.validate_on_submit():
		spreadsheet_id = form.identifier.data
		upd_frequency = int(os.environ.get('UPD_FREQUENCY'))
		tables = [fio_noname, fio_ugolovniki, fio_Renesans, fio_Alfa, fio_BRR, 
						fio_rozysk_avto, fio_rozysk, fio_grant01, fio_grant02, 
						fio_grant03, fio_otkaz01, fio_otkaz02, fio_digbank, fio_rosfin]
		req_sheets = ['fio_noname', 'fio_ugolovniki', 'fio_Renesans', 'fio_Alfa', 'fio_BRR', 
						'fio_rozysk_avto', 'fio_rozysk', 'fio_grant01', 'fio_grant02', 
						'fio_grant03', 'fio_otkaz01', 'fio_otkaz02', 'fio_digbank', 'fio_rosfin']
		req_columns = [
		#fio_noname
		['otvetchik', 'istec', 'category', 'date'],
		#fio_ugolovniki
		['ame01', 'name02', 'name03', 'date'],
		#fio_Renesans
		['name01', 'name02', 'name03', 'number', 'summ'],
		#fio_Alfa
		['fio', 'prezhnee_fio', 'summa_zayavki', 'dni_prosrochki', 
		'summa_osn_dolga', 'summa_prosroch_dolga', 'data_postupl_zayavki'],
		#fio_BRR
		['name', 'summ'],
		#fio_rozysk_avto
		['number', 'name', 'pistav01', 'UFSSP_OSP', 'data_zaveden_dela', 'nomer_dela', 'pristav02', 
		'data_zapret_snyat_s_ucheta', 'marka_model', 'gos_nomer', 'VIN', 'nomer_kuzova', 'cvet', 
		'podrazdel_gibdd', 'rekvisity'],
		#fio_rozysk
		['number', 'name', 'birthday', 'mesto_rozhd', 'cont_tel', 'otdel_pristavov'],
		#fio_grant01
		['company01', 'deyatelnost', 'company02'],
		#fio_grant02
		['num01', 'num02', 'name', 'company', 'goal', 'contacts'],
		#fio_grant03
		['number', 'col01', 'col02', 'col03', 'col04'],
		#fio_otkaz01
		['name', 'col01', 'col02', 'col03'],
		#fio_otkaz02
		['date', 'name', 'col01', 'col02', 'col03', 'col04', 'col05'],
		#fio_digbank
		['naimen_imushestva', 'buh_otchet', 'faktich_nalich'],
		#fio_rosfin
		['name', 'number', 'place']
		]
		service = sv.google_sheet_api_init()
		if not sv.check_file(service, spreadsheet_id, req_sheets, req_columns):
			return redirect(url_for('.import_fio', query_type = 'wrong_file'))
		query_type = 'data_loaded'
		tabs_data = []
		for i in range(len(req_sheets)):
			col_number = len(req_columns[i])
			range_name = 'A1:' + sv.get_excel_col_by_index(col_number)
			gs_rows = sv.get_google_sheet_rows(service, spreadsheet_id, req_sheets[i], range_name)
			db.session.query(tables[i]).delete()
			if len(gs_rows) == 1:
				tabs_data.append([req_sheets[i], 0])
				continue
			gs_rows = gs_rows[1:]
			try:
				sv.convert_rows_to_mysql(db, tables[i], gs_rows, col_number, upd_frequency)
			except:
				return redirect(url_for('.import_fio', query_type = 'wrong_file'))
			tabs_data.append([req_sheets[i], db.session.query(tables[i]).count()])
		tabs_number = len(tabs_data)
		return render_template('import_google_sheets.html', dataset_name = 'По ФИО', query_type = query_type,
										tabs_number = tabs_number, tabs_data = tabs_data)
	return render_template('import_google_sheets.html', form = form, dataset_name = 'По ФИО',
										query_type = query_type, tabs_number = 0, tabs_data = [])

################################################


