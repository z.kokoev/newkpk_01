import pandas as pd
import httplib2
import apiclient.discovery
from oauth2client.service_account import ServiceAccountCredentials
import time





class Google_sheets_table:

	def __init__(self, credentials_file, spreadsheet_id):
		scopes = ['https://www.googleapis.com/auth/spreadsheets', 
					'https://www.googleapis.com/auth/drive']
		status = 'Success'
		try:
			credentials = ServiceAccountCredentials.from_json_keyfile_name(
				credentials_file, scopes)
		except:
			service = {}
			metadata = {}
			sheet_names = {}
			spreadsheet_name = {}
			status = 'Error: credentials'
		if status == 'Success':
			httpAuth = credentials.authorize(httplib2.Http())
			service = apiclient.discovery.build('sheets', 'v4', http = httpAuth)
			try:
				metadata = service.spreadsheets().get(spreadsheetId = spreadsheet_id).execute()
			except:
				metadata = {}
				sheet_names = {}
				spreadsheet_name = {}
				status = 'Error: spreadsheet_id'
			if status == 'Success':
				spreadsheet_name = metadata['properties']['title']
				sheet_names = tuple(x['properties']['title'] for x in metadata.get('sheets'))
		self.service = service
		self.spreadsheet_id = spreadsheet_id
		self.metadata = metadata
		self.sheet_names = sheet_names
		self.spreadsheet_name = spreadsheet_name
		self.status = status

	def get_sheet_data(self, sheet_name):
		if sheet_name not in self.sheet_names:
			return 'Error: Wrong sheet_name'
		source_data = self.service.spreadsheets().values().get(
			spreadsheetId = self.spreadsheet_id, range = sheet_name).execute()
		if not source_data.get('values'):
			return pd.DataFrame()
		data = source_data['values']
		headers = data.pop(0)
		try:
			res = pd.DataFrame(data, columns = headers)
		except Exception as e:
			return f'Error: in sheet <{sheet_name}> {str(e)}'
		return res


	def __repr__(self):
		return (f'{self.__class__.__name__}:\n'
				f'service: {self.service!r},\n'
				f'spreadsheet_id: {self.spreadsheet_id!r},\n'
				f'metadata: {self.metadata!r},\n'
				f'spreadsheet_name: {self.spreadsheet_name!r},\n'
				f'sheet_names: {self.sheet_names!r},\n'
				f'status: {self.status!r}\n')





def get_excel_col_by_index(n):
	if n > 52:
		raise Exception('Are you kidding me?')
	if n <= 26:
		return chr(n + 64)
	res = 'A' + chr(n + 38)
	return res


def google_sheet_api_init():
	CREDENTIALS_FILE = 'credentials.json'
	credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_FILE, 
	['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive'])
	httpAuth = credentials.authorize(httplib2.Http())
	return apiclient.discovery.build('sheets', 'v4', http = httpAuth)

def get_google_sheet_rows(service, spreadsheet_id, sheet_name, range_name):
	full_range_name = sheet_name + '!' + range_name
	try:
		res = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id, range=full_range_name).execute()
	except:
		return False
	return res.get('values')

def check_file(service, spreadsheet_id, req_sheets, req_columns):
	for i in range(len(req_sheets)):
		range_name = 'A1:' + get_excel_col_by_index(len(req_columns[i])) + '1'
		labels = get_google_sheet_rows(service, spreadsheet_id, req_sheets[i], range_name)
		if not labels:
			return False
		if labels[0] != req_columns[i]:
			return False
	return True

def convert_rows_to_mysql(db, table_name, gs_rows, col_number, upd_frequency):
	rows = []
	for i in range(len(gs_rows)):
		row = gs_rows[i]
		l_row = len(row)
		if l_row < col_number:
			row.extend([None]*(col_number - l_row))
		if l_row > col_number:
			row = row[:col_number]
			l_row = col_number
		for j in range(l_row):
			if not row[j]:
				row[j] = None
		new_row = table_name(*row)
		rows.append(new_row)
		if (i % upd_frequency == 0):
			db.session.add_all(rows)
			db.session.commit()
			rows = []
			time.sleep(0.2)
	if not rows:
		return i
	db.session.add_all(rows)
	db.session.commit()
	return i

		 