from flask_table import Table, Col

class TestTable(Table):
    col_01 = Col('col_01')
    col_02 = Col('col_02')
    col_03 = Col('col_03')

class Logs_Table(Table):
    user_name = Col('user_name')
    query_text = Col('query_text')
    query_date = Col('query_date')
    query_time = Col('query_time')

class Sp_per_Table(Table):
    name = Col('name')
    inf = Col('inf')
    dop_inf = Col('dop_inf')
    main = Col('main')
    born_in = Col('born_in')
    birthday = Col('birthday')
    passport = Col('passport')
    place = Col('place')
    object_type = Col('object_type')

class Mir_sudy_Table(Table):
    number = Col('number')
    name = Col('name')
    uchastok = Col('uchastok')
    date = Col('date')
    note = Col('note')

class Ug_Sovet_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    name = Col('name')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Ug_Promysh_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    name = Col('name')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Ug_Prigorod_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    name = Col('name')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Ug_Prvoberezh_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    name = Col('name')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Ug_Mozdok_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    name = Col('name')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Ug_Lenin_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    name = Col('name')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Ug_Kirov_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    name = Col('name')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Ug_Iraf_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    name = Col('name')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Ug_Digor_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    name = Col('name')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Ug_Ardon_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    name = Col('name')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Ug_Alagir_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    name = Col('name')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Sud_Al_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    category = Col('category')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Sud_Ard_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    category = Col('category')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Sud_Verh_i_Vl_Gar_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    category = Col('category')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Sud_Dig_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    category = Col('category')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Sud_Iraf_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    category = Col('category')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Sud_Kir_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    category = Col('category')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Sud_Len_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    category = Col('category')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Sud_Mozd_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    category = Col('category')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Sud_Prav_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    category = Col('category')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Sud_Prig_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    category = Col('category')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Sud_Prom_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    category = Col('category')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Sud_Sov_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    category = Col('category')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Sud_pr_iski_Sov_Table(Table):
    number = Col('number')
    date_01 = Col('date_01')
    category = Col('category')
    some_inf = Col('some_inf')
    date_02 = Col('date_02')
    comment = Col('comment')
    date_03 = Col('date_03')
    more_inf = Col('more_inf')

class Fio_noname_Table(Table):
    otvetchik = Col('otvetchik')
    istec = Col('istec')
    category = Col('category')
    date = Col('date')

class Fio_ugolovniki_Table(Table):
    name01 = Col('name01')
    name02 = Col('name02')
    name03 = Col('name03')
    date = Col('date')

class Fio_Renesans_Table(Table):
    name01 = Col('name01')
    name02 = Col('name02')
    name03 = Col('name03')
    number = Col('number')
    summ = Col('summ')
    date = Col('date')
    col_g = Col('col_g')
    col_h = Col('col_h')
    col_i = Col('col_i')

class Fio_Alfa_Table(Table):
    fio = Col('fio')
    prezhnee_fio = Col('prezhnee_fio')
    summa_zayavki = Col('summa_zayavki')
    dni_prosrochki = Col('dni_prosrochki')
    summa_osn_dolga = Col('summa_osn_dolga')
    summa_prosroch_dolga = Col('summa_prosroch_dolga')
    data_postupl_zayavki = Col('data_postupl_zayavki')

class Fio_BRR_Table(Table):
    name = Col('name')
    summ = Col('summ')

class Fio_rozysk_avto_Table(Table):
    number = Col('number')
    name = Col('name')
    pistav01 = Col('pistav01')
    UFSSP_OSP = Col('UFSSP_OSP')
    data_zaveden_dela = Col('data_zaveden_dela')
    nomer_dela = Col('nomer_dela')
    pristav02 = Col('pristav02')
    data_zapret_snyat_s_ucheta = Col('data_zapret_snyat_s_ucheta')
    marka_model = Col('marka_model')
    gos_nomer = Col('gos_nomer')
    VIN = Col('VIN')
    nomer_kuzova = Col('nomer_kuzova')
    cvet = Col('cvet')
    podrazdel_gibdd = Col('podrazdel_gibdd')
    rekvisity = Col('rekvisity')

class Fio_rozysk_Table(Table):
    number = Col('number')
    name = Col('name')
    birthday = Col('birthday')
    mesto_rozhd = Col('mesto_rozhd')
    cont_tel = Col('cont_tel')
    otdel_pristavov = Col('otdel_pristavov')

class Fio_grant01_Table(Table):
    company01 = Col('company01')
    deyatelnost = Col('deyatelnost')
    company02 = Col('company02')

class Fio_grant02_Table(Table):
    num01 = Col('num01')
    num02 = Col('num02')
    name = Col('name')
    company = Col('company')
    goal = Col('goal')
    contacts = Col('contacts')

class Fio_grant03_Table(Table):
    number = Col('number')
    col01 = Col('col01')
    col02 = Col('col02')
    col03 = Col('col03')
    col04 = Col('col04')

class Fio_otkaz01_Table(Table):
    name = Col('name')
    col01 = Col('col01')
    col02 = Col('col02')
    col03 = Col('col03')

class Fio_otkaz02_Table(Table):
    date = Col('date')
    name = Col('name')
    col01 = Col('col01')
    col02 = Col('col02')
    col03 = Col('col03')
    col04 = Col('col04')
    col05 = Col('col05')

class Fio_digbank_Table(Table):
    naimen_imushestva = Col('naimen_imushestva')
    buh_otchet = Col('buh_otchet')
    faktich_nalich = Col('faktich_nalich')

class Fio_rosfin_Table(Table):
    name = Col('name')
    number = Col('number')
    place = Col('place')